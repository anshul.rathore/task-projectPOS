import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from './Components/home.js';
import Adminlogin from './Components/adminLogin.js';
import Dashboard from './Components/Dashboard.js';
function App() {
  return (
  	<Router>
  	<div>
  	<Switch>
  	<Route path="/" exact component={Home} />
  	<Route path="/adminSource" component={Adminlogin}/>
  	<Route path="/adminDashboard/:id" component={Dashboard}/>
  	</Switch>
  	</div>
  	</Router>
   
  );
}

export default App;
