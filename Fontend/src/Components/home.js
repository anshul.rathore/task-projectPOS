import React, { Component } from "react";
import { Link } from "react-router-dom";
import NavHome from "./Views/homenavbar.js";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ParticlesBg from "particles-bg";

export default class Home extends Component {
  componentDidMount() {
    toast.success("Welcome To My App!", {
      position: "bottom-center",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
  }
  render() {
    return (
      <div className="super-container">
        <ToastContainer
          position="bottom-center"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        <NavHome active={"home"} />
        <section style={{ height: "700px" }}>
          <h2 className="text-center pt-5">
            Welsome To{" "}
            <span className="font-weight-bold text-primary">My App</span>.{" "}
            <br />
            <Link to="/adminSource">Signup</Link> Now If you are not connected
            with us.
          </h2>
        </section>
        <ParticlesBg type="cobweb" bg={true} />
      </div>
    );
  }
}
