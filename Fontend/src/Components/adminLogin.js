import React, { Component } from "react";
import { Link } from "react-router-dom";
import NavHome from "./Views/homenavbar.js";
import { If, Then, ElseIf, Else } from "react-if-elseif-else-render";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as EmailValidator from "email-validator";
import axios from "axios";
import ParticlesBg from "particles-bg";

export default class Adminlogin extends Component {
  constructor(prop) {
    super(prop);
    this.onChange = this.onChange.bind(this);
    this.onLogin = this.onLogin.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {
      fName: "",
      lName: "",
      phone: "",
      address1: "",
      address2: "",
      city: "",
      state: "",
      country: "",
      zip: "",
      email: "",
      pass: "",
      cpass: "",
      toggle: 1,
    };
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  onLogin(e) {
    e.preventDefault();
    if (EmailValidator.validate(this.state.email) && this.state.pass !== "") {
      let loginData = {
        email: this.state.email,
        pass: this.state.pass,
      };
      axios
        .post("http://localhost:8000/admin/adminLogin", loginData)
        .then((res) => {
          if (res.data.message === "email") {
            toast.error("Invalid Email!", {
              position: "bottom-center",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          } else if (res.data.message === "password") {
            toast.error("Invalid Password!", {
              position: "bottom-center",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          } else {
            console.log(res.data);
            toast.success("Please Wait! Logging You in!", {
              position: "bottom-center",
              autoClose: 2500,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
            localStorage.setItem("myapAdmn", res.data.token);

            setTimeout(
              () =>
                (window.location.href =
                  "/adminDashboard/" + res.data.result._id),
              3000
            );
          }
        });
    } else if (this.state.pass === "") {
      toast.error("All Fields Are Required!", {
        position: "bottom-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    } else {
      toast.error("Enter Valid Email!", {
        position: "bottom-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  }

  onSubmit(e) {
    e.preventDefault();
    if (
      this.state.fName !== "" &&
      this.state.lName !== "" &&
      this.state.phone !== "" &&
      this.state.address1 !== "" &&
      this.state.city !== "" &&
      this.state.country !== "" &&
      this.state.state !== "" &&
      this.state.zip !== ""
    ) {
      if (this.state.pass.length > 6) {
        if (this.state.pass === this.state.cpass) {
          if (EmailValidator.validate(this.state.email)) {
            let addData = {
              fname: this.state.fName,
              lname: this.state.lName,
              email: this.state.email,
              phone: this.state.phone,
              address: this.state.address1 + " " + this.state.address2,
              city: this.state.city,
              country: this.state.country,
              zipCode: this.state.zip,
              pass: this.state.pass,
            };

            axios
              .post("http://localhost:8000/admin/addAdmin", addData)
              .then((res) => {
                if (res.data.statusCode === 200) {
                  toast.success("Account Has Been Created !", {
                    position: "bottom-center",
                    autoClose: 2500,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                  });
                  setTimeout(
                    () => this.setState({ toggle: 0, email: "", pass: "" }),
                    3000
                  );
                } else if (res.data.err.keyPattern.phone === 1) {
                  toast.error("Phone Number Already Exist !", {
                    position: "bottom-center",
                    autoClose: 2500,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                  });
                } else if (res.data.err.keyPattern.email === 1) {
                  toast.error("Email Already Exist !", {
                    position: "bottom-center",
                    autoClose: 2500,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                  });
                }
              });
          } else {
            toast.error("Please Enter Valid Email !", {
              position: "bottom-center",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          }
        } else {
          toast.error("Password Didnt Matched !", {
            position: "bottom-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
      } else {
        toast.error("Password Length Must Be Min 6", {
          position: "bottom-center",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    } else {
      toast.error("All Fields Are Required!", {
        position: "bottom-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  }

  render() {
    console.log(EmailValidator.validate("an@gmail.com"));
    return (
      <div className="super-container">
        <ToastContainer
          position="bottom-center"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        <NavHome active={"login"} />
        <section className="pt-5 container  ">
          <If condition={this.state.toggle === 0}>
            <Then>
              <div
                className="card shadow"
                style={{
                  height: "450px",
                  paddingTop: "70px",
                  backgroundColor: "#ffffff66",
                }}
              >
                <h3 className="text-center pb-3">Admin Login</h3>
                <form className="row">
                  <div className=" col-md-4"></div>
                  <div className="form-group col-md-4">
                    <input
                      type="email"
                      className="form-control"
                      placeholder="Enter email"
                      name="email"
                      onChange={this.onChange}
                      value={this.state.email}
                    />
                  </div>
                  <div className=" col-md-4"></div>
                  <div className=" col-md-4"></div>
                  <div className="form-group col-md-4">
                    <input
                      type="password"
                      className="form-control"
                      placeholder="Password"
                      name="pass"
                      onChange={this.onChange}
                      value={this.state.pass}
                    />
                  </div>
                  <div className=" col-md-4"></div>

                  <div className="col-md-4"></div>
                  <div
                    className="btn btn-primary col-md-4"
                    onClick={this.onLogin}
                  >
                    Submit
                  </div>
                  <div className="col-md-4"></div>
                  <br />
                  <div className="col-md-4"></div>
                  <Link
                    className="col-md-4 pt-2"
                    onClick={() =>
                      this.setState({ toggle: 1, email: "", pass: "" })
                    }
                  >
                    Create An Account?
                  </Link>
                  <div className="col-md-4"></div>
                </form>
              </div>
            </Then>
            <ElseIf condition={this.state.toggle === 1}>
              <div
                className="card shadow"
                style={{
                  height: "500px",
                  paddingTop: "70px",
                  backgroundColor: "#ffffff66",
                }}
              >
                <h3 className="text-center pb-3">Admin Signup</h3>
                <form className="row">
                  <div className=" col-md-2"></div>
                  <div className="form-group col-md-4">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="First Name *"
                      name="fName"
                      onChange={this.onChange}
                      value={this.state.fName}
                    />
                  </div>
                  <div className="form-group col-md-4">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Last Name *"
                      name="lName"
                      onChange={this.onChange}
                      value={this.state.lName}
                    />
                  </div>
                  <div className=" col-md-2"></div>
                  <div className=" col-md-2"></div>
                  <div className="form-group col-md-4">
                    <input
                      type="email"
                      className="form-control"
                      placeholder="Email *"
                      name="email"
                      onChange={this.onChange}
                      value={this.state.email}
                    />
                  </div>
                  <div className="form-group col-md-4">
                    <input
                      type="number"
                      className="form-control"
                      placeholder="Phone No *"
                      name="phone"
                      onChange={this.onChange}
                      value={this.state.phone}
                    />
                  </div>
                  <div className=" col-md-2"></div>
                  <div className=" col-md-2"></div>
                  <div className="form-group col-md-4">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Address Line1 *"
                      name="address1"
                      onChange={this.onChange}
                      value={this.state.address1}
                    />
                  </div>
                  <div className="form-group col-md-4">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Address Line 2"
                      name="address2"
                      onChange={this.onChange}
                      value={this.state.address2}
                    />
                  </div>
                  <div className=" col-md-2"></div>
                  <div className="col-md-2"></div>
                  <div className=" col-md-2">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="City *"
                      name="city"
                      onChange={this.onChange}
                      value={this.state.city}
                    />
                  </div>
                  <div className="form-group col-md-2">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="State *"
                      name="state"
                      onChange={this.onChange}
                      value={this.state.state}
                    />
                  </div>
                  <div className=" col-md-2">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Zip/Pin Code *"
                      name="zip"
                      onChange={this.onChange}
                      value={this.state.zip}
                    />
                  </div>
                  <div className=" col-md-2">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Country *"
                      name="country"
                      onChange={this.onChange}
                      value={this.state.country}
                    />
                  </div>
                  <div className="col-md-2"></div>
                  <div className=" col-md-2"></div>
                  <div className="form-group col-md-4">
                    <input
                      type="password"
                      className="form-control"
                      placeholder="Password *"
                      name="pass"
                      onChange={this.onChange}
                      value={this.state.pass}
                    />
                  </div>
                  <div className="form-group col-md-4">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Confirm Password *"
                      name="cpass"
                      onChange={this.onChange}
                      value={this.state.cpass}
                    />
                  </div>
                  <div className=" col-md-2"></div>
                  <div className="col-md-4"></div>
                  <div
                    className="btn btn-primary col-md-4 btn-block"
                    onClick={this.onSubmit}
                  >
                    Create Account
                  </div>
                  <div className="col-md-4"></div>
                  <br />
                  <div className="col-md-4"></div>
                  <Link
                    className="col-md-4 pt-2"
                    onClick={() =>
                      this.setState({ toggle: 0, email: "", pass: "" })
                    }
                  >
                    Already have an Account?
                  </Link>
                  <div className="col-md-4"></div>
                </form>
              </div>
            </ElseIf>
          </If>
        </section>
        <ParticlesBg type="cobweb" bg={true} />
      </div>
    );
  }
}
