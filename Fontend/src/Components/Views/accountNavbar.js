import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class NavAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      homeClass: "nav-item ",
      login: "nav-item ",
    };
  }

  logout() {
    localStorage.removeItem("myapAdmn");
    window.location.href = "/";
  }

  render() {
    console.log(this.props);
    return (
      <div className="super-container">
        <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
          <Link className="navbar-brand">My App</Link>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarNavDropdown">
            <ul className="navbar-nav">
              <li className="nav-item active">
                <Link
                  className="nav-link"
                  style={{ paddingLeft: "1040px" }}
                  to="/"
                >
                  Welcome Back
                </Link>
              </li>
              <li className="nav-item ">
                <Link className="nav-link" onClick={this.logout.bind(this)}>
                  Logout
                </Link>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}
