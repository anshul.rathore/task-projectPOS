import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class NavHome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      homeClass: "nav-item ",
      login: "nav-item ",
    };
  }

  componentDidMount() {
    if (this.props.active === "home") {
      this.setState({ homeClass: "nav-item active" });
    } else if (this.props.active === "login") {
      this.setState({ login: "nav-item active" });
    }
  }

  render() {
    console.log(this.props);
    return (
      <div className="super-container">
        <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
          <Link className="navbar-brand">My App</Link>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarNavDropdown">
            <ul className="navbar-nav">
              <li className={this.state.homeClass}>
                <Link
                  className="nav-link"
                  style={{ paddingLeft: "1040px" }}
                  to="/"
                >
                  Home <span className="sr-only">(current)</span>
                </Link>
              </li>
              <li className={this.state.login}>
                <Link className="nav-link" to="/adminSource">
                  SignUp
                </Link>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}
