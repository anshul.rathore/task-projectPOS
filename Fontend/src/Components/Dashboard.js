import React, { Component } from "react";
import { Link } from "react-router-dom";
import NavAccount from "./Views/accountNavbar.js";
import { If, Then, ElseIf } from "react-if-elseif-else-render";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as EmailValidator from "email-validator";
import axios from "axios";

export default class Dashboard extends Component {
  constructor(prop) {
    super(prop);
    this.onChange = this.onChange.bind(this);
    this.onChangeProduct = this.onChangeProduct.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onAddProduct = this.onAddProduct.bind(this);
    this.addtoCard = this.addtoCard.bind(this);
    this.state = {
      fName: "",
      lName: "",
      phone: "",
      address1: "",
      city: "",
      state: "",
      country: "",
      zip: "",
      email: "",
      pass: "",
      pName: "",
      pPrice: null,
      pQty: null,
      pTotal: null,
      selectProduct: "",
      toggle: 0,
      products: [],
      myProduct: [],
      toggleSof: 0,
    };
  }

  componentDidMount() {
    if (localStorage.myapAdmn === undefined) {
      window.location.href = "/adminSource";
    } else {
      axios
        .get("http://localhost:8000/product/listProducts")
        .then((res) => this.setState({ products: res.data.result }));
      toast.success("Welcome Back!", {
        position: "bottom-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  onChangeProduct(e) {
    const find = e.target.value.toLocaleLowerCase();
    const filteredItems = this.state.products.filter(
      (item) => item.pName.toLocaleLowerCase() === find
    );

    const itemsToDisplay = find ? filteredItems : this.state.products;
    this.setState({ myProduct: itemsToDisplay, toggleSof: 1 });
  }

  onSubmit(e) {
    e.preventDefault();
    if (
      this.state.fName !== "" &&
      this.state.lName !== "" &&
      this.state.phone !== "" &&
      this.state.address1 !== "" &&
      this.state.city !== "" &&
      this.state.country !== "" &&
      this.state.state !== "" &&
      this.state.zip !== ""
    ) {
      if (this.state.pass.length >= 6) {
        if (EmailValidator.validate(this.state.email)) {
          let addData = {
            fname: this.state.fName,
            lname: this.state.lName,
            email: this.state.email,
            phone: this.state.phone,
            address: this.state.address1,
            city: this.state.city,
            country: this.state.country,
            zipCode: this.state.zip,
            pass: this.state.pass,
          };

          axios
            .post("http://localhost:8000/customer/addCustomer", addData)
            .then((res) => {
              if (res.data.statusCode === 200) {
                toast.success("Customer Added", {
                  position: "bottom-center",
                  autoClose: 5000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });
              } else if (res.data.err.keyPattern.email === 1) {
                toast.error("Email Already Exist", {
                  position: "bottom-center",
                  autoClose: 5000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });
              } else if (res.data.err.keyPattern.phone === 1) {
                toast.error("Phone Already Exist", {
                  position: "bottom-center",
                  autoClose: 5000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });
              }
            });
        } else {
          toast.error("Please Enter Valid Email !", {
            position: "bottom-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
      } else {
        toast.error("Password Length Must Be Min 6", {
          position: "bottom-center",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    } else {
      toast.error("All Fields Are Required!", {
        position: "bottom-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  }

  onAddProduct(e) {
    e.preventDefault();
    if (this.state.pName === "" && this.state.pPrice === null) {
      toast.error("All Fields Are Required!", {
        position: "bottom-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    } else {
      let product = {
        pName: this.state.pName,
        pPrice: this.state.pPrice,
      };
      axios
        .post("http://localhost:8000/product/addProduct", product)
        .then((res) => {
          if (res.data.statusCode === 200) {
            toast.success("Product Added!", {
              position: "bottom-center",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          } else if (res.data.err.keyPattern.pName === 1) {
            toast.error("Product Already Exist!", {
              position: "bottom-center",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          }
        });
    }
  }

  addtoCard(e) {
    e.preventDefault();
    let addtocard = {
      pName: this.state.myProduct[0].pName,
      pPrice: this.state.myProduct[0].pPrice,
      pQty: this.state.pQty,
      grandTotal: this.state.pTotal,
      pId: this.state.myProduct[0]._id,
      cName: "random",
      cId: this.props.location.pathname.slice(0 - 24),
    };
    axios
      .post("http://localhost:8000/product/addTocart", addtocard)
      .then((res) => {
        if (res.data.statusCode === 200) {
          toast.success(
            "Order Placed Succesfully for Product : " +
              this.state.myProduct[0].pName +
              "for Quantity : " +
              this.state.pQty,
            {
              position: "bottom-center",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            }
          );
        } else {
          toast.error("Something is wrong Please Try Again!", {
            position: "bottom-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
      });
  }

  render() {
    return (
      <div className="super-container">
        <ToastContainer
          position="bottom-center"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        <NavAccount name={"AR"} />
        <section className="pt-5 container">
          <div className="row" style={{ height: "90px" }}>
            <Link
              className="col-md-4 card shadow text-center font-weight-bold text-white"
              style={{ paddingTop: "30px", backgroundColor: "#b63fc4db" }}
              onClick={() =>
                this.setState({
                  toggle: 1,
                })
              }
            >
              Add Customer
            </Link>
            <Link
              className="col-md-4 card shadow text-center font-weight-bold text-white"
              style={{ paddingTop: "30px", backgroundColor: "#35cecebf" }}
              onClick={() =>
                this.setState({
                  toggle: 2,
                })
              }
            >
              Add Product
            </Link>
            <Link
              className="col-md-4 card shadow text-center font-weight-bold text-white"
              style={{ paddingTop: "30px", backgroundColor: "#35e332" }}
              onClick={() =>
                this.setState({
                  toggle: 3,
                })
              }
            >
              Make Order
            </Link>
          </div>
          <If condition={this.state.toggle === 1}>
            <Then>
              <div className="card shadow pt-4" style={{ height: "500px" }}>
                <h4 className="text-center">Add Customer</h4>
                <br />
                <form className="row">
                  <div className="form-group col-md-6">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="First Name"
                      name="fName"
                      value={this.state.fName}
                      onChange={this.onChange}
                    />
                  </div>
                  <div className="form-group col-md-6">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Last Name"
                      name="lName"
                      value={this.state.lName}
                      onChange={this.onChange}
                    />
                  </div>
                  <div className="form-group col-md-6">
                    <input
                      type="email"
                      className="form-control"
                      placeholder="Email"
                      name="email"
                      value={this.state.email}
                      onChange={this.onChange}
                    />
                  </div>
                  <div className="form-group col-md-6">
                    <input
                      type="number"
                      className="form-control"
                      id="inputPassword4"
                      placeholder="phone"
                      name="phone"
                      value={this.state.phone}
                      onChange={this.onChange}
                    />
                  </div>

                  <div className="form-group col-md-12">
                    <input
                      type="text"
                      className="form-control"
                      id="inputAddress"
                      placeholder="Address"
                      name="address1"
                      value={this.state.address1}
                      onChange={this.onChange}
                    />
                  </div>

                  <div className="form-group col-md-3">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="City"
                      name="city"
                      value={this.state.city}
                      onChange={this.onChange}
                    />
                  </div>
                  <div className="form-group col-md-3">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="State"
                      name="state"
                      value={this.state.state}
                      onChange={this.onChange}
                    />
                  </div>
                  <div className="form-group col-md-3">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Country"
                      name="country"
                      value={this.state.country}
                      onChange={this.onChange}
                    />
                  </div>
                  <div className="form-group col-md-3">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Zip/Pin Code"
                      name="zip"
                      value={this.state.zip}
                      onChange={this.onChange}
                    />
                  </div>

                  <div className="form-group col-md-12">
                    <input
                      type="password"
                      className="form-control"
                      id="inputAddress"
                      placeholder="Password"
                      name="pass"
                      value={this.state.pass}
                      onChange={this.onChange}
                    />
                  </div>
                  <div className="col-md-4"></div>
                  <div
                    className="btn btn-primary col-md-4"
                    onClick={this.onSubmit}
                  >
                    Add Customer
                  </div>
                  <div className="col-md-4"></div>
                </form>
              </div>
            </Then>
            <ElseIf condition={this.state.toggle === 2}>
              <div className="card shadow pt-4" style={{ height: "220px" }}>
                <h4 className="text-center">Add Product</h4>
                <br />
                <form className="row">
                  <div className="form-group col-md-6">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Product Name"
                      name="pName"
                      value={this.state.pName}
                      onChange={this.onChange}
                    />
                  </div>

                  <div className="form-group col-md-6">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Product Price"
                      name="pPrice"
                      value={this.state.pPrice}
                      onChange={this.onChange}
                    />
                  </div>

                  <div className="col-md-4"></div>
                  <div
                    className="btn btn-primary col-md-4"
                    onClick={this.onAddProduct}
                  >
                    Add Product
                  </div>
                  <div className="col-md-4"></div>
                </form>
              </div>
            </ElseIf>
            <ElseIf condition={this.state.toggle === 3}>
              <div className="card shadow pt-4" style={{ height: "250px" }}>
                <h4 className="text-center">Make Order</h4>
                <br />
                <form className="row">
                  <div className="form-group col-md-4">
                    <label>Product Name</label>
                    <select
                      className="custom-select my-1 mr-sm-2"
                      name="selectProduct"
                      onChange={this.onChangeProduct}
                    >
                      <option selected>Avialable Products</option>
                      {this.state.products.map((current, i) => (
                        <option key={i}>{current.pName}</option>
                      ))}
                    </select>
                  </div>
                  {this.state.myProduct.map((current, i) => (
                    <>
                      <div className="form-group col-md-4">
                        <label>Product Price</label>
                        <input
                          type="number"
                          className="form-control"
                          placeholder="Price"
                          name="pPrice"
                          value={current.pPrice}
                          disabled
                        />
                      </div>
                      <div className="form-group col-md-4">
                        <label>Quantity</label>
                        <input
                          type="number"
                          className="form-control"
                          placeholder="Quantity"
                          name="pQty"
                          value={this.state.pQty}
                          onChange={(e) =>
                            this.setState({
                              pTotal: e.target.value * current.pPrice,
                              pQty: e.target.value,
                            })
                          }
                        />
                      </div>
                      <If
                        condition={
                          this.state.pTotal !== null || this.state.pTotal !== 0
                        }
                      >
                        <Then>
                          <div className="col-md-4"></div>
                          <div className="col-md-2">
                            <label>Total : </label>
                            {this.state.pTotal}
                          </div>
                          <div
                            className="btn btn-primary col-md-2"
                            onClick={this.addtoCard}
                          >
                            Add To Cart
                          </div>
                          <div className="col-md-4"></div>
                        </Then>
                      </If>
                    </>
                  ))}
                </form>
              </div>
            </ElseIf>
          </If>
        </section>
      </div>
    );
  }
}
