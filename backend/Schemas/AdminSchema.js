const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let AdminSchema = new Schema({
  fname: {
    type: String,
    required: true,
  },
  lname: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  phone: {
    type: String,
    required: true,
    unique: true,
  },
  address: {
    type: String,
    required: true,
  },
  city: {
    type: String,
    required: true,
  },
  country: {
    type: String,
    required: true,
  },
  zipCode: {
    type: String,
  },
  pass: {
    type: String,
    required: true,
  },
});

let Admin = mongoose.model("Admin", AdminSchema);
module.exports = {
  Admin: Admin,
};
