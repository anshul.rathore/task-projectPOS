const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let CustomerSchema = new Schema({
  fname: {
    type: String,
    required: true,
  },
  lname: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  phone: {
    type: String,
    required: true,
    unique: true,
  },
  address: {
    type: String,
    required: true,
  },
  city: {
    type: String,
    required: true,
  },
  country: {
    type: String,
    required: true,
  },
  zipCode: {
    type: String,
  },
  pass: {
    type: String,
    required: true,
  },
});

let messageSchema = new Schema(
  {
    text: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

let MessageMeSchema = new Schema({
  message: [messageSchema],
});

let Customer = mongoose.model("Customer", CustomerSchema);
let MessageMe = mongoose.model("MessageMe", MessageMeSchema);
module.exports = {
  Customer: Customer,
  MessageMe: MessageMe,
};
