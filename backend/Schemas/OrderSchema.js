const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let OrderSchema = new Schema({
  pName: {
    type: String,
  },
  pPrice: {
    type: Number,
  },
  pQty: {
    type: String,
  },
  grandTotal: {
    type: Number,
  },
  pId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Product",
  },
  cName: {
    type: String,
  },
  cId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Admin",
  },
});

let Order = mongoose.model("Order", OrderSchema);
module.exports = {
  Order: Order,
};
