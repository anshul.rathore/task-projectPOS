const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let ProductSchema = new Schema({
  pName: {
    type: String,
    required: true,
    unique: true,
  },
  pPrice: {
    type: Number,
  },
});

let Product = mongoose.model("Product", ProductSchema);
module.exports = {
  Product: Product,
};
