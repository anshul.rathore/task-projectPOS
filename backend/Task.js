const { send } = require("process");

const express = require("express"),
  app = express(),
  cors = require("cors"),
  bodyparser = require("body-parser"),
  mongoose = require("mongoose"),
  userRoutes = express.Router(),
  jwt = require("jsonwebtoken"),
  PORT = 8000;

//Importing Collection Schemas
(Admin = require("./Schemas/AdminSchema")),
  (Customer = require("./Schemas/CustomerSchema")),
  (Product = require("./Schemas/ProductSchema")),
  (Order = require("./Schemas/OrderSchema.js")),
  mongoose.set("useCreateIndex", true);

//Importing Routes
var Admin = require("./Routes/Admin.js"),
  Customer = require("./Routes/Customer.js"),
  Product = require("./Routes/Product.js");

//Importing Modules with express
app.use(cors());
app.use(bodyparser.json());
app.use("/uploads", express.static("uploads"));
app.use(bodyparser.urlencoded({ extended: true }));
app.use(userRoutes);
app.use("/admin", Admin);
app.use("/customer", Customer);
app.use("/product", Product);

//Setting Up Collection with mongoose
mongoose.connect("mongodb://127.0.0.1:27017/task1", {
  useUnifiedTopology: true,
  useNewUrlParser: true,
});
const connection = mongoose.connection;

//Creating Connection with DB
connection.once("open", function () {
  console.log("MongoDB connection established!");
});

//Listening Server
app.listen(PORT, function () {
  console.log("App runnning on " + PORT);
});
