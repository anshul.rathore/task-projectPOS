const express = require("express"),
  app = express(),
  cors = require("cors"),
  bodyparser = require("body-parser"),
  mongoose = require("mongoose"),
  productRoutes = express.Router(),
  jwt = require("jsonwebtoken"),
  process = require("process"),
  envir = process.env;

app.use(cors());
app.use(bodyparser.urlencoded({ extended: true }));
app.use(productRoutes);

productRoutes.get("/listProducts", (req, res) => {
  //List All Products
  mongoose
    .model("Product")
    .find()
    .then((result) => {
      res.send({ result: result, statusCode: 200 });
    })
    .catch((err) => {
      res.send({ err: err, statusCode: 400 });
    });
});

productRoutes.get("/Listboughtproducts", (req, res) => {
  //List All Bought Products
  mongoose
    .model("Order")
    .find()
    .then((result) => {
      res.send({ result: result, statusCode: 200 });
    })
    .catch((err) => {
      res.send({ err: err, statusCode: 400 });
    });
});

productRoutes.get("/listproductspecific/:id", (req, res) => {
  //Get Product Specific Sales
  mongoose
    .model("Order")
    .find({ pId: req.params.id })
    .then((result) => {
      res.send({ result: result, statusCode: 200 });
    })
    .catch((err) => {
      res.send({ err: err, statusCode: 400 });
    });
});

productRoutes.post("/addProduct", (req, res) => {
  //Signup Product
  let Product = new mongoose.model("Product")({
    pName: req.body.pName,
    pPrice: req.body.pPrice,
  });
  Product.save()
    .then((result) => {
      res.send({ result: result, statusCode: 200 });
    })
    .catch((err) => {
      res.send({ err: err, statusCode: 400 });
    });
});

productRoutes.post("/addTocart", (req, res) => {
  //Add To Cart Product
  let addToCart = new mongoose.model("Order")({
    pName: req.body.pName,
    pPrice: req.body.pPrice,
    pQty: req.body.pQty,
    grandTotal: req.body.grandTotal,
    pId: req.body.pId,
    cName: req.body.cName,
    cId: req.body.cId,
  });
  addToCart
    .save()
    .then((result) => {
      res.send({ result: result, statusCode: 200 });
    })
    .catch((err) => {
      res.send({ err: err, statusCode: 400 });
    });
});

module.exports = productRoutes;
