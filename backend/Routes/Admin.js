const express = require("express"),
  app = express(),
  cors = require("cors"),
  bodyparser = require("body-parser"),
  mongoose = require("mongoose"),
  adminRoutes = express.Router(),
  jwt = require("jsonwebtoken"),
  process = require("process"),
  envir = process.env;

const { body, validationResult } = require("express-validator");

app.use(cors());

app.use(bodyparser.urlencoded({ extended: true }));
app.use(adminRoutes);

adminRoutes.get("/listAdmins", (req, res) => {
  //List All Admins
  mongoose
    .model("Admin")
    .find()
    .then((result) => {
      res.send({ result: result, statusCode: 200 });
    })
    .catch((err) => {
      res.send({ err: err, statusCode: 400 });
    });
});

adminRoutes.post("/test", [body("email").isEmail()], (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ errors: errors.array() });
  } else {
    res.send("OK");
  }
});

adminRoutes.post("/addAdmin", (req, res) => {
  //Signup Admin
  let admin = new mongoose.model("Admin")({
    fname: req.body.fname,
    lname: req.body.lname,
    email: req.body.email,
    phone: req.body.phone,
    address: req.body.address,
    city: req.body.city,
    country: req.body.country,
    zipCode: req.body.zipCode,
    pass: req.body.pass,
  });
  admin
    .save()
    .then((result) => {
      res.send({ result: result, statusCode: 200 });
    })
    .catch((err) => {
      res.send({ err: err, statusCode: 400 });
    });
});

adminRoutes.post("/adminLogin", (req, res) => {
  //Admin Login
  mongoose.model("Admin").findOne({ email: req.body.email }, (err, result) => {
    if (result !== null) {
      if (result.pass === req.body.pass) {
        const myAdmin = {
          _id: result._id,
          fname: result.fname,
          lname: result.lname,
          email: result.email,
          phone: result.phone,
          address: result.address,
          city: result.city,
          country: result.country,
          zipCode: result.zipCode,
          pass: result.pass,
        };
        jwt.sign(
          { myAdmin },
          "secretkey",
          { expiresIn: "3000s" },
          (err, token) => {
            res.json({
              token,
              result,
              message: "LOGGIN",
            });
          }
        );
      } else {
        res.send({ message: "password" });
      }
    } else {
      res.send({ message: "email" });
    }
  });
});

function verifyToken(req, res, next) {
  //Token Verification Function
  const bearerHeader = req.headers["authorization"];
  if (typeof bearerHeader !== "undefined") {
    const bearer = bearerHeader.split(" ");
    const bearerToken = bearer[1];
    req.token = bearerToken;
    next();
  } else {
    res.send({ Message: "Failed" });
  }
}

module.exports = adminRoutes;
