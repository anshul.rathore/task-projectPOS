const express = require("express"),
  app = express(),
  cors = require("cors"),
  bodyparser = require("body-parser"),
  mongoose = require("mongoose"),
  userRoutes = express.Router(),
  process = require("process"),
  envir = process.env;

app.use(cors());
app.use(bodyparser.urlencoded({ extended: true }));
app.use(userRoutes);

userRoutes.get("/listCustomers", (req, res) => {
  //List All Customers
  mongoose
    .model("Customer")
    .find()
    .then((result) => {
      res.send({ result: result, statusCode: 200 });
    })
    .catch((err) => {
      res.send({ err: err, statusCode: 400 });
    });
});

userRoutes.post("/messageMe", (req, res) => {
  //Message Me Post
  let message = new mongoose.model("MessageMe")({
    message: [
      {
        text: req.body.text1,
      },
      {
        text: req.body.text2,
      },
    ],
  });
  message
    .save()
    .then((result) => {
      res.status(200).json({ result: result });
    })
    .catch((err) => {
      res.send(400).json({ err: err });
    });
});

userRoutes.get("/getMessage", (req, res) => {
  //Get Message
  mongoose
    .model("MessageMe")
    .find()
    .then((result) => {
      console.log(res);
      res.status(200).json({ result: result, statusCode: 200 });
    })
    .catch((err) => {
      res.sendStatus(400).json({ err: err, statusCode: 200 });
    });
});

userRoutes.post("/addCustomer", (req, res) => {
  //Signup Customer
  let Customer = new mongoose.model("Customer")({
    fname: req.body.fname,
    lname: req.body.lname,
    email: req.body.email,
    phone: req.body.phone,
    address: req.body.address,
    city: req.body.city,
    country: req.body.country,
    zipCode: req.body.zipCode,
    pass: req.body.pass,
  });
  Customer.save()
    .then((result) => {
      res.send({ result: result, statusCode: 200 });
    })
    .catch((err) => {
      res.send({ err: err, statusCode: 400 });
    });
});

module.exports = userRoutes;
